# Sunandan Nandi
----
![DP](profile_pic/SUNANDAN.jpg "Sunandan Nandi")

I believe on job learning is the best way to learn any new concept.
I have completed my bachelor in Information technology. 
I prefer to work in a challenging environment. I like to work on c++.I prefer to work in a challenging environment.

## Siemens Technology India 
Lead Research Engineer
Dates EmployedDec 2021 – Present
Employment Duration2 mos
LocationBangalore Urban, Karnataka, India


## Leica Geosystems part of Hexagon
Technical Lead
Company NameLeica Geosystems part of Hexagon
Dates Employed Mar 2020 – Dec 2021
Employment Duration1 yr 10 mos
Location Hyderabad, Telangana
Worked on developing IOT solution for BLK scanners and 3D visualization.

## Cognizant
Senior Associate
Dates EmployedOct 2018 – Feb 2020
Employment Duration1 yr 5 mos
LocationHyderabad Area, India
Working on middleware development focused in the areas of Industrial Automation , Process Control.

## Mindteck
Module Lead
Company Name Mindteck
Dates EmployedSep 2016 – Oct 2018
Employment Duration2 yrs 2 mos
LocationKolkata Area, India
Working on advanced c++ in multi threaded environment. Worked on Medical device for monitoring vital signs.
Honeywell Technology Solutions, Inc.


## Honeywell Technology Solutions, Inc.
Total Duration2 yrs 6 mos
Senior Software Engineer
Dates EmployedJan 2016 – Sep 2016
Employment Duration9 mos
Location Karnataka, India
Worked on windows Flight simulator software.
Title Engineer
Full-time
Dates EmployedApr 2014 – Dec 2015
Employment Duration1 yr 9 mos
Location Karnataka, India


## Wipro Technologies
Total Duration2 yrs 11 mos
Title Software Engineer
Dates EmployedApr 2013 – Apr 2014
Employment Duration1 yr 1 mo
`Worked in a automotive connectivity project` as member of Bluetooth team.Learning some advanced technologies.Have knowledge of Hands Free Profile of car infotainment system.
TitleSoftware Engineer
Dates EmployedJun 2011 – Apr 2013
Employment Duration1 yr 11 mos
`Worked in Network Application and Management` Protocols (Hewlet Packard) project which is based on Network application development, enhancement and maintenance of networking products like TCP/IP, FTP.
